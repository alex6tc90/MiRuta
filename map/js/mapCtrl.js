/* Modulo del mapa que permite la representacion del mismo en la pagina y que cuenta con los datos
   necesarios para esto */
(function () {
	'use strict';

	angular.module('mapModule', ['ui.bootstrap', 'ngMap', 'moduleAppServices'])
		 .controller('MapController', ['$scope','ConnectionDBService', MapController]);

	function MapController(vm, servWebDB){
         vm.map = {
           	   center: {
           			latitude: -42.77000141404137,
           			longitude: -65.0339126586914
           		},
           		zoom: 13,
           		options : {
           			scrollwheel: false
           		},
           		control: {}
          };

          // recuperamos algunos puntos de interes desde el servidor para mostrarlos en el mapa
          servWebDB.getPuntoInteres(
            function(err, res){
               if(err){
                    console.log(err);
        		      return alert('Ocurrió un error al buscar los puntos de interes: ' + err);
        		}
               console.log(res);
        	      vm.puntoRecuperadosDB = res;
        	   });

           // ejemplo de datos que se esperan
           /* vm.puntoRecuperadosDB = [
                   { nombre : "MiCasa", latitud : "-42.75806696157168", longitud : "-65.04571788620016"},
                   { nombre : "LaCurva", latitud : "-42.756696278748294", longitud :"-65.04548185180687"},
                   { nombre : "Cancha", latitud : "-42.74479999941405", longitud :"-65.06082408737144"}
          ];*/
	}
})()
