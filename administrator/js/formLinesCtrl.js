/* Controlador del formulario del usuario que brinda funcionalidad y validacion de sus componentes */
(function () {
	'use strict';

	angular.module('adminModule')
		.controller('FormLinesCtrl', ['$scope', '$routeParams', '$location','AdminSrv', FormLinesCtrl]);

	function FormLinesCtrl(vm, params, location, service){
		vm.line = {};

		// validaciones
		vm.onlyLetters = /^[a-zA-Z_áéíóúñ\s]*$/ ;
		vm.onlyNumbers = /^([0-9])*$/;

		// datos mostrados en una lista desplegables
		vm.empresas = [
	          'Benitez',
	          'Ceferino'
	        ];

		vm.resetLine = function(){
			vm.line = {}
		}

		// guardamos una linea
		vm.updateLine = function(){
			service.saveLine(vm.line, function(err, res){
				if(err){
					return alert('Ocurrió un error: ' + err.data)
				}
				alert('Se guardó correctamente la linea: ' + res.id)
				vm.line.id = res.id
			})
		}

		// busca una linea por su id
		vm.searchLine = function(){
			service.findLineById(vm.lineId, function(err, res){
				if(err){
					return alert('Ocurrió un error buscando una linea: ' + err)
				}
				vm.line = res;
			})
		}

		// vamos a buscar la linea a la base de datos, con su id, para mostrar sus datos
		if(params.id&& !isNaN(params.id)){
			vm.lineId = parseInt(params.id);
			vm.searchLine();
		}

		// redireccion
		vm.go = function ( path ) {
       location.path( path );
    };
	}
})()
