/* Modulo del administrador con el routing y el controlador con los datos que permiten la correcta
   visualizacion de la pantalla principal del mismo */

(function () {
	'use strict';

	angular.module('adminModule', ['puntosinterest'])
          .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
           $routeProvider
                   .when('/user/:id', {
                       templateUrl: 'administrator/partials/formUser.html',
                       controller: 'FormUsersCtrl'
                   })
                   .when('/user', {
                       templateUrl: 'administrator/partials/formUser.html',
                       controller: 'FormUsersCtrl'
                   })
                   .when('/line', {
                       templateUrl: 'administrator/partials/formLine.html',
                       controller: 'FormLinesCtrl'
                   })
                   .when('/line/:id', {
                       templateUrl: 'administrator/partials/formLine.html',
                       controller: 'FormLinesCtrl'
                   })
                   .when('/administrator', {
                       templateUrl: 'administrator/partials/admin.html'
                   });
            }])
		 .controller('AdminController', ['$scope', '$http','$location','AdminSrv', AdminController])
           //filtro para poner la primera letra en mayúscula
           .filter("mayus", function(){
             return function(text) {
                 if(text != null){
                    return text.substring(0,1).toUpperCase()+text.substring(1);
                 }
             }
            });

     function AdminController(vm, http, location,serviceAdmin ){
       vm.infoTabs = [
            { title:"Usuarios", template:"administrator/partials/users.html"},
            { title:"Transportes",template:"administrator/partials/transportation.html"},
            { title:"Puntos de Interes", template:"administrator/partials/interest.html" }
       ];

       vm.dataTableUser = { nombre: "Nombre", apellido: "Apellido", email: "Email"};

       vm.dataTableTransportation = { nombre: "Nombre", frecuencia: "Frecuencia(min)", boleto: "PrecioBoleto", empresa: "Empresa"};

       // redirecciona
       vm.go = function ( path ) {
            console.log("entro al go");
           location.path( path );
       };

     }
})()
