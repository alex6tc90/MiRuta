/* Controlador del formulario de linea, que brinda funcionalidad y validacion de sus componentes */
(function () {
	'use strict';

	angular.module('adminModule')
		.controller('FormUsersCtrl', ['$scope', '$routeParams', '$location','AdminSrv', FormUsersCtrl]);

	function FormUsersCtrl(vm, params, location, service){
		vm.user = {};

		// patrones de validacion
		vm.onlyLetters = /^[a-zA-Z_áéíóúñ\s]*$/ ;
		vm.emailVal = /[^\@]+\@[^\.]+\..+/;

		vm.resetUser = function(){
			vm.user = {}
		}

		// guardamos un usuario
		vm.updateUser = function(){
			service.saveUser(vm.user, function(err, res){
				console.log('datos del usuario: '+ vm.user);
				if(err){
					return alert('Ocurrió un error: ' + err.data)
				}
				alert('Se guardó correctamente el usuario: ' + res.id)
				vm.user.id = res.id
			})
		}

		vm.searchUser = function(){
			service.findUserById(vm.userId, function(err, res){
				if(err){
					return alert('Ocurrió un error buscando un usuario: ' + err)
				}
				vm.user = res;
			})
		}

		// vamos a buscar el usuario a la base de datos, con su id para mostrar sus datos
		if(params.id&& !isNaN(params.id)){
			vm.userId = parseInt(params.id);
			vm.searchUser();
		}

		// redireccion
		vm.go = function ( path ) {
       location.path( path );
    };

	}
})()
