/* Controlador que permite visualizar la lista de usuarios en la pantalla de la misma, recuperados del servidor */

(function () {
	'use strict';

	angular.module('adminModule')
		.controller('ListUsersCtrl', ['$scope', 'AdminSrv', ListUsersCtrl]);

	function ListUsersCtrl(vm, service){
		// obtenemos los usuarios desde el servidor
	  service.findAllUsers(function(err, res){
	 			if(err){
	 				return alert('Ocurrió un error buscando un usuario: ' + err)
	 			}
	 		vm.dataUsers = res;
	    console.log(res);
	 	});

			// ejemplo de datos que se esperan recuperar del servidor
		  /*vm.dataUsers = [
		             { id:1, nombre: "alex", apellido: "torrico", email:"alex_6@hotmail.com"},
		             { id:2, nombre: "ariel", apellido: "aguirre", email:"arielA@hotmail.com"},
		             { id:3, nombre: "joaquin", apellido: "lima", email:"ema@hotmail.com"}
		  ]; */
		}
})()
