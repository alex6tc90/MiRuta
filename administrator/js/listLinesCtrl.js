/* Controlador que permite visualizar la lista de lineas en la pantalla de la misma, recuperadas del servidor */
(function () {
	'use strict';

	angular.module('adminModule')
		.controller('ListLinesCtrl', ['$scope', 'AdminSrv', ListLinesCtrl]);

    function ListLinesCtrl(vm, service){
		// obtenemos los usuarios desde el servidor
	    service.findAllLines(function(err, res){
	 		if(err){
	 			return alert('Ocurrió un error buscando una linea: ' + err)
	 		}
	 		vm.dataLines = res;
	    console.log(res);
	 	});

        // ejemplo de datos que se esperan
         /*vm.dataLines = [
             { id:1, nombre: "linea1", frecuencia: 20, boleto: 9.0, empresa: "Benitez"},
             { id:2, nombre: "linea2", frecuencia: 20, boleto: 9.0, empresa: "Ceferino"},
             { id:3, nombre: "linea3", frecuencia: 20, boleto: 9.0, empresa: "Benitez"}
         ];*/
	}
})()
