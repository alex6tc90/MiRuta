/* Servicios de recoleccion de los datos a mostrar en la aplicacion, obtenidos desde el servidor */
(function () {
	'use strict';

	angular.module('adminModule')
		.factory('AdminSrv', ['$http', function($http){
			return {

				/******************* servicios de usuario **************************/
				/*******************************************************************/
				saveUser: function(user, callback){
					$http
						.post('http://192.168.0.237:4000/usuario', user)
						.then(
							function(res){
								return callback(false, res.data)
							},
							function(err){
								return callback(err)
							}
						)
				},
				findUserById: function(id, callback){
					$http
						.get('http://192.168.0.237:4000/usuario/'+id)
						.then(
							function(res){
								return callback(false, res.data)
							},
							function(err){
								return callback(err.data)
							}
						)
				},
				findAllUsers: function(callback){
						$http.get('http://192.168.0.237:4000/listaUsuarios')
								 .then(
									 // success
									 function(res){
										 return(callback(false, res.data))
									 },
									 // error
									 function(data, status, header, config){
										 console.log("No se pudo cargar la lista de usuarios, error # " + status);
									 }
								 )
				},

				/******************** servicios de lineas **************************/
				/*******************************************************************/
				saveLine: function(line, callback){
					$http
						.post('http://192.168.0.237:4000/linea', line)
						.then(
							function(res){
								return callback(false, res.data)
							},
							function(err){
								return callback(err)
							}
						)
				},
				findLineById: function(id, callback){
					$http
						.get('http://192.168.0.237:4000/linea/'+id)
						.then(
							function(res){
								return callback(false, res.data)
							},
							function(err){
								return callback(err.data)
							}
						)
				},
				findAllLines: function(callback){
						$http.get('http://192.168.0.237:4000/listaLineas')
								 .then(
									 // success
									 function(res){
										 return(callback(false, res.data))
									 },
									 // error
									 function(data, status, header, config){
										 console.log("No se pudo cargar la lista de lineas, error # " + status);
									 }
								 )
				}
			}
	}])
})()
