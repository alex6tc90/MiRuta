(function () {
  'use strict';

  	angular.module('puntosinterest', ['ui.bootstrap', 'ngMap'])
		.controller('interestController', ['$scope', '$http', function ($scope, $http) {
  			// Variable usada para almacenar la lista de provincias que vienen del servidor web
		    $scope.listaProvincias = null;

		    // Variable usada cuando el usuario selecciona una provincia
		    $scope.provinciaElegida = null;

		    $http.get('http://192.168.0.237:4000/listaProvincias')
		    	.then(
		    		// success
					function(res){
						$scope.listaProvincias = res.data

						if($scope.listaProvincias.length > 0) {
		                    // Selecciona la primera provincia si es que hay elementos en la lista
		                	$scope.provinciaElegida = $scope.listaProvincias[0].provinciaID;

		                	// Cargamos la lista de ciudades y sus puntos de interes
		                	$scope.cargarCiudad();
						}
					},
					// error
					function(data, status, header, config){
						$scope.errorMessage = "No se pudo cargar la lista de provincias, error # " + status;
					}
				)

			// Si el usuario hace clic en un <div>, esta funcion se invoca (por ng-click) y establecemos la provincia elegida
		    $scope.elegirProvincia = function (val) {
		        $scope.provinciaElegida = val.provinciaID;
		        $scope.cargarCiudad();
		    }

		    $scope.cargarCiudad = function () {
		        // Limpiamos la lista de ciudades
		        $scope.listaCiudades = null;

		        // Traemos la lista de ciudades que se corresponden con la provincia que se selecciono
		        $http.get('http://192.168.0.237:4000/listaCiudades/' + $scope.provinciaElegida)
		        	.then(
		        		// success
						function(res){
							$scope.listaCiudades = res.data;
						},
						// error
						function(data, status, header, config){
							$scope.errorMessage = "No se pudo cargar la lista de ciudades, error # " + status;
						}
				)
		    }
		}]);
	
	// function controller(scope, http){
		
	// }
})()

