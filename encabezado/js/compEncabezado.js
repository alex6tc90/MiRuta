/* componente que representa el encabezado(llamado desde la vista por la directiva con su mismo nombre) con
    partial correspondiente */

(function () {
	'use strict';
	angular.module('Encabezado', [])
	.component('encabezado', {
		templateUrl: 'encabezado/partials/encabezado.html'
	})
})()
