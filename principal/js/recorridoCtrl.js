/* Controlador que brinda los datos necesarios que se visualizan en la pantalla de la opcion
   de "Busqueda de recorrido" de la pantalla de inicio */
(function () {
	'use strict';

	angular.module('principalModule')
		.controller('ControladorOpcionesBusqueda', ['$scope', ControladorOpcionesBusqueda]);
	function ControladorOpcionesBusqueda(vm){
        vm.opcionesRecorrido = [
          'A pie',
          'Bicicleta',
          'Auto',
          'Colectivo'
        ];
        vm.defaultOption = 'Auto';
	}
})()
