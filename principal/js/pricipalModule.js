/* Modulo principal de la aplicacion con el ruteo general de la aplicacion */
(function () {
	'use strict';
	angular
		.module('principalModule', [])
		.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
                $routeProvider
                        .when('/inicioSesion', {
                            templateUrl: 'principal/partials/inicioSesion.html'
                        })
                        .when('/registro', {
                            templateUrl: 'principal/partials/registro.html'
                        })
                        .when('/inicio', {
                            templateUrl: 'principal/partials/inicio.html',
                            controller: 'ControladorInicio',
                        })
                        .when('/administrator', {
                            templateUrl: 'administrator/partials/admin.html'
                        });
            }]);
})()
