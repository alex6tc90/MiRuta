/* Controlador de la pantalla de inicio de la aplicacion que permite visualizar diferentes opciones de
   funcionamiento de la aplicacion al usuario (opciones solo con la visual, sin funcionamiento aun) */
(function () {
	'use strict';

	angular.module('principalModule')
		.controller('ControladorInicio', ['$scope', ControladorInicio]);

	function ControladorInicio(vm){
		vm.tabs = [
      { title:"Busqueda de recorridos", template:"principal/partials/opciones.html"},
      { title:"Transportes",template:"principal/partials/transportes.html"},
      { title:"Informacion",template:"principal/partials/informacion.html" }
    ];
	}
})()
