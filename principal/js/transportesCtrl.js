/* Controlador que brinda los datos necesarios que se visualizan en la pantalla de la opcion
   de "Transportes" de la pantalla de inicio */
(function () {
	'use strict';

	angular.module('principalModule')
		.controller('ControladorTransportes', ['$scope', ControladorTransportes]);
	function ControladorTransportes(vm){
        vm.lineasTransportes = [
          'Linea 1',
          'Linea 2',
          'Linea 3',
          'Linea 4',
          'Linea 5'
        ];
        vm.defaulLineOption = 'Linea 1';
        vm.diasFuncionamiento = 'Lun - Sab';
        vm.horarios = '7 AM a 22.30 PM';
	}
})()
