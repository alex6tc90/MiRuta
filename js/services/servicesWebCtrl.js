/* Controlador que se encarga de traer los datos requeridos por el mapa */
(function () {
	'use strict';

	angular.module('moduleAppServices')
		.controller('ServiceWebController', ['$scope', 'ConnectionDBService', ServiceWebController]);

		function ServiceWebController(vm, servWebDB){
			var $ = vm;
			$.puntos = []
			angular.extend($,{
				puntosInteres: function(){
					servWebDB.getPuntoInteres(function(err, res){
						if(err){
							return alert('Ocurrió un error al traer los puntos de interes ' + err)
						}
						/* aca vendrian los datos de los puntos de interes*/
						$.puntos = res;
					});
				}
			})
		}

})()
