/* Servicio que recupera datos del servidor para verlos reflejados en el mapa */
(function () {
	'use strict';

	angular.module('moduleAppServices')
		.factory('ConnectionDBService', ['$http', function($http){
			return {
				getPuntoInteres: function(callback){
					$http
						.get('http://192.168.0.237:4000/marcadores')
						.then(
							function(res){
								return callback(false, res.data);
							},
							function(data, status, header, config){
								return callback(data);
							}
						)
				}
			}
		}
	]);
})()
