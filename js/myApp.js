// modulo principal con sus respectivas dependencias
(function () {
	'use strict';
	angular.module('myApp', [
		'ngAnimate',
		'ngSanitize',
		'ngRoute',
		'ngMap',
		'ui.bootstrap',
		'MyRoutes',
		'Encabezado',
		'principalModule',
		'mapModule',
		'moduleAppServices',
		'adminModule',
		'puntosinterest'
	]).run(['$rootScope',
		function($rootScope){
			$rootScope.title = "--- MiRuta_v1 ---";
		}])
})();
